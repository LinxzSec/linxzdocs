# .library-ms Analysis

Library files are a way of viewing the contents of multiple directories in a single view. A library can contain both files and folders stored on a local computer or in a remote location. 

Windows library files by default are located in `%APPDATA%\Roaming\Microsoft\Windows\Libraries` with the file extension of: "library-ms". These files are XML based and follow a public schema. An example of such a file, in this case "Documents" can be found below.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<libraryDescription xmlns="http://schemas.microsoft.com/windows/2009/library">
  <name>@shell32.dll,-34575</name>
  <ownerSID>S-1-5-21-1273487106-3773811068-1740608888-1000</ownerSID>
  <version>2</version>
  <isLibraryPinned>true</isLibraryPinned>
  <iconReference>imageres.dll,-1002</iconReference>
  <templateInfo>
    <folderType>{7d49d726-3c21-4f05-99aa-fdc2c9474656}</folderType>
  </templateInfo>
  <searchConnectorDescriptionList>
    <searchConnectorDescription publisher="Microsoft" product="Windows">
      <description>@shell32.dll,-34577</description>
      <isDefaultSaveLocation>true</isDefaultSaveLocation>
      <simpleLocation>
        <url>knownfolder:{FDD39AD0-238F-46AF-ADB4-6C85480369C7}</url>
        <serialized>MBAAAEAFCAAAAAAAADAAAAAAAY0gAAQDRAAAAkdy1YnTOddA+azq45kTXHQWUjKeO501BAAEAAAAAAAABAAAAAAAAAAAAAAAAAAAAMXAUAwHQB+TQDi66kGEiiNCAsCMw0ZKAECAAAAAAAAAAAAAAAAAAAAAAAAAAAAA3bXDISdEIcEqC6ZljLZVHBFAxAAAAAAAAAAAAABAVNXZyNHA8AQCAQAAv7LAAAAAAAAAA4CAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUFAzBQZAIHAzBAAAQBAQBQMAAAAAAAAAAAAQAAbp5Ge6BAPAkAAEAw7+CAAAAAAAAAAuAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAsBQaA4GA4BgeAAAAUAAlAEDAAAAAAUrUdRXEAQ0TDVVTF5XMAAAfAkAAEAw7+WrUbRXtS1FduAAAAQhbBAAAAEAAAAAAAAAAAAgQAAAAAAwpADKAEBwbAMGA1BQbAUGAuBAdAMHAAAAQAcHApBgbAQGAvBwdAMHAuAwcAQHAvBgcAEGAnBQZA4CAkBAbAwGAsAQLAIDAxAwNAcDAwAAAAgBAAAwRAAAAcAAAAEAAAAAHAAAAtAAAAAAAAAgRAAAARAAAAMAAAAwpteE3QAAAAAwQ6wVVzVmczxFbp5Ge6xFRvNWdtVmb0NHAAABAAAQBAAAoFAAAAEXAAAAHAAAALAAAgCtmT3/jj8qRtSLbFi0ApdccBAAAgBAAAMAAAAKWAAAAAAAAAQWZztGdvBXL2sGN1YjMpBglxOOwN96OIR4NUVZw0WTA/p9XpSourHhuyBADpMAbtYZsjDcTvuDSEeDVVGMt1Ewfa/VqEq76RorcAwQKDwWLSDAAAkAAAAajAAAAxMFUTJuiYZEvMhzQ7y/ETaCmt5ccAAAAEAAAAAwHAAAAvAAAAMFAtAQMA0CA1AQLAIDAxAQLAEDAyAwNAMDA0AAOAcDAxAAMAYDAtAwMAcDA3AwMAgDAxAQMAADA2AAOA0CAxAwNAQDAwAgNAADA4AAOAgDA4AQLAEDAwAAMAADAAAAAAAAAAAQOAAAAxMFUTFrFtRUrNCHSniEQuQaP4xYHAAAAoBAAAAASAAAA3bXDISdEIcEqC6ZljLZVHBAAAAAAAAAAAAAAAA</serialized>
      </simpleLocation>
    </searchConnectorDescription>
  </searchConnectorDescriptionList>
</libraryDescription>
```

## Reverse Engineering Windows.Storage.dll

We begin our reverse engineering at windows.storage.dll. Windows.storage.dll, also known as Microsoft WinRT Storage API, is an essential DLL for the functioning of the Windows filesystem. It has a lot of important functionality, some of which relating to .library-ms files. Hence we begin our analysis from here.

Upon loading the DLL into IDA and performing a quick filter for functions associated with the word "library", one function instantly stands out: `GetXP_Map_libraryDescription` the reason for this function standing out is due to the fact its argument is `XMLPROP_ITEM`. This function name & argument seems to fit with the `libraryDescription` line from our XML file that can be found above. Let's investigate this function further.

### GetXP_Map_libraryDescription

```
; __int64 __fastcall GetXP_Map_libraryDescription(const struct XMLPROP_ITEM **)
?GetXP_Map_libraryDescription@@YAJPEAPEBUXMLPROP_ITEM@@@Z proc near
```

In the first block (shown below) we see a `mov rax, cs` instruction which moves `p_xpMap_libraryDescription` into the RAX register. This is quite interesting since this suggests that the `libraryDescription` XML key value might be moved into the RAX register. But, since a jump zero follows this instruction, we can actually assume that this is merely a check that we have returned into this function correctly, since that is typically what RAX is used for at the end of a block. Meaning, is everything okay in order to continue.

![test](./2021-06-09_16-18.png)

So far we can assume that the purpose of this function is likely to retrieve all of the keys and fields from a given .library-ms file. This is further confirmed by the fact that assuming we take the jump following the `test rax, rax` then we hit a basic block which again references the `libraryDescription` XML key. The reason why the libraryDescription key is important is due to the fact that it also contains the schema for the XML file, `xmlns="http://schemas.microsoft.com/windows/2009/library"`.

In the same block, there is also an interesting reference to another function `CLibraryDescription::s_GetPropertyStore`. However, that function does take `XMLPROP_ITEM` as an argument so for now we can assume that it is not *that* important, but perhaps something to come back to.

![](./2021-06-09_16-33.png)

At this point, we are pretty confident with our assumptions about this function and if we continue to move through blocks we will see XML keys & fields being referenced multiple times. As reverse engineers, we would like to understand *why* and where this leads and at the same time, it might be nice to think about security vulnerabilities which can exist in these sorts of functions. For example, memory corruption, unitialized memory use, things like that.

I am going to load explorer.exe into WinDbg so that I can set a breakpoint on this function and analyse the content of the registers dynamically while I also review the assembly statically. This enables me to get a clearer understanding of how functions are functioning. Since the function is located in windows.storage.dll, we will have to set our breakpoint to reflect that.

```
0:042> bp windows_storage!GetXP_Map_libraryDescription
breakpoint 0 redefined
0:042> g
Breakpoint 0 hit
windows_storage!GetXP_Map_libraryDescription:
00007fff`58b50b78 488bc4          mov     rax,rsp
```

As shown in the above code block, we sucessfully created a breakpoint on the function. In order to trigger it we can simply open file explorer and navigate to the aforementioned location (`%APPDATA%\Roaming\Microsoft\Windows\Libraries`) here we can just double click any of the library files and our breakpoint will trigger.

As shown once hitting the breakpoint, we have arrived at the first instruction. Let's go ahead and single step through the instructions to see what we can learn following the function prologue.

```
0:053> p
windows_storage!GetXP_Map_libraryDescription+0x3:
00007fff`58b50b7b 48895810        mov     qword ptr [rax+10h],rbx ds:00000000`0885bb08=000000000885bb69

...

0:053> 
windows_storage!GetXP_Map_libraryDescription+0x38:
00007fff`58b50bb0 488b05a93b7000  mov     rax,qword ptr [windows_storage!p_xpMap_libraryDescription (00007fff`59254760)] ds:00007fff`59254760=0000000000000000
```

As expected, RAX will contain zero in preparation for the jump and test. If we take a look at the call stack at this point in time, we can learn that the function which we arrived from was `CLibraryDescription::Load`. This is shown in the below call stack output.

![](./2021-06-09_16-54.png)

Based on this we can assume that anything from `CLibrary` is going to be interesting to us going forward. It is clear to us that this current function is for mapping the fields from the `libraryDescription` key. With this in mind, our next goal is to further understand the process after mapping these fields. In order to figure this out we can continue to analyse the blocks in IDA and follow them in WinDbg.

If we continue following the execution, we will arrive in the basic block we referenced before (shown below), which is the first block which seems to interact with the `libraryDescription` key from the XML file.

![](./2021-06-10_12-52.png)

Digging into `CLibraryDescription::s_GetPropertyStore` is quite interesting, since in a few of the blocks we see references to static GUIDs. When looking at these GUIDs in the .data section, we can see some comments which give some insight into what these specific GUIDs might be used for. The most interesting one being `_GUID_71604b0f_97b0_4764_8577_2f13e98a1422`.

![](./2021-06-10_13-06.png)

At this stage, it is quite hard to say what this does. Especially since our XML document does not reference any GUID field, which suggests that these GUIDs are defined by Microsoft and are unique to each type of library file. For now, let's continue through the blocks.

The next set of blocks go over the fields in the XML file, this is expected so we won't go over every single check, we already can be pretty confident on the purpose of this function based on previous analysis. One such example block is featured below.

![](./2021-06-10_15-24.png)

In this block, a pointer to the string "description" is moved into the RAX register via load effective address, this is followed by that value being moved into RDI+10, then the property key for this `FileDescription` field is moved into the RAX register followed by that value being moved into RDI+20. This is of course the setup for required fields from the XML file. This sequence repeats for each of the fields, such as OwnerSID, name, version, etc. The reason for the property key is essentially a unique value to match the fields with a specific key value. For example, `FileDescription` has a property key value of `0CEF7D53h`, this can be found in the .data section, as shown in the below image.

![](./2021-06-10_15-28.png)

These property keys are important to the functionality of not just this feature but many other file formats within Windows. At the end of all of these PKEYs being moved into registers to be used later, we arrive at the block shown below which calls the function `XMLPROP_ITEM`, this stands out. Based on the name we can assume that this might perhaps setup something to be used later in the application with the fields from our XML file. Let's look into this.

![](./2021-06-10_16-07.png)

Interestingly, if we look through following blocks, we can see multiple references to this function, and if we move far enough down we can also learn that it is in-fact a structure which is used in a call to `GetXP_Map_searchConnectorDescription(XMLPROP_ITEM const * *)` this gives us the idea that potentially this structure is a collection of the items from our XML file such as the name, version, description, etc.

![](./2021-06-17_14-52.png)

If we step into this function in IDA, this theory can actually be confirmed. Since we see keys from the XML document being associated with PKEYs, which we already discussed is how Windows maps these fields for future reference during the execution of the application. Additionally, based on the function name, we know this function is dedicated to parsing the XML fields from the "searchConnectorDescription" key of the XML file. This includes information such as the URL of the folder, its description, the publisher, etc. This is confirmed in our XML file, repeated again below.

```xml
<searchConnectorDescription publisher="Microsoft" product="Windows">
      <description>@shell32.dll,-34577</description>
      <isDefaultSaveLocation>true</isDefaultSaveLocation>
      <simpleLocation>
        <url>knownfolder:{FDD39AD0-238F-46AF-ADB4-6C85480369C7}</url>
        <serialized>MBAAAEAFCAAAAAAAADAAAAAAAY0gAAQDRAAAAkdy1YnTOddA+azq45kTXHQWUjKeO501BAAEAAAAAAAABAAAAAAAAAAAAAAAAAAAAMXAUAwHQB+TQDi66kGEiiNCAsCMw0ZKAECAAAAAAAAAAAAAAAAAAAAAAAAAAAAA3bXDISdEIcEqC6ZljLZVHBFAxAAAAAAAAAAAAABAVNXZyNHA8AQCAQAAv7LAAAAAAAAAA4CAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUFAzBQZAIHAzBAAAQBAQBQMAAAAAAAAAAAAQAAbp5Ge6BAPAkAAEAw7+CAAAAAAAAAAuAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAsBQaA4GA4BgeAAAAUAAlAEDAAAAAAUrUdRXEAQ0TDVVTF5XMAAAfAkAAEAw7+WrUbRXtS1FduAAAAQhbBAAAAEAAAAAAAAAAAAgQAAAAAAwpADKAEBwbAMGA1BQbAUGAuBAdAMHAAAAQAcHApBgbAQGAvBwdAMHAuAwcAQHAvBgcAEGAnBQZA4CAkBAbAwGAsAQLAIDAxAwNAcDAwAAAAgBAAAwRAAAAcAAAAEAAAAAHAAAAtAAAAAAAAAgRAAAARAAAAMAAAAwpteE3QAAAAAwQ6wVVzVmczxFbp5Ge6xFRvNWdtVmb0NHAAABAAAQBAAAoFAAAAEXAAAAHAAAALAAAgCtmT3/jj8qRtSLbFi0ApdccBAAAgBAAAMAAAAKWAAAAAAAAAQWZztGdvBXL2sGN1YjMpBglxOOwN96OIR4NUVZw0WTA/p9XpSourHhuyBADpMAbtYZsjDcTvuDSEeDVVGMt1Ewfa/VqEq76RorcAwQKDwWLSDAAAkAAAAajAAAAxMFUTJuiYZEvMhzQ7y/ETaCmt5ccAAAAEAAAAAwHAAAAvAAAAMFAtAQMA0CA1AQLAIDAxAQLAEDAyAwNAMDA0AAOAcDAxAAMAYDAtAwMAcDA3AwMAgDAxAQMAADA2AAOA0CAxAwNAQDAwAgNAADA4AAOAgDA4AQLAEDAwAAMAADAAAAAAAAAAAQOAAAAxMFUTFrFtRUrNCHSniEQuQaP4xYHAAAAoBAAAAASAAAA3bXDISdEIcEqC6ZljLZVHBAAAAAAAAAAAAAAAA</serialized>
      </simpleLocation>
    </searchConnectorDescription>
```

Based on this we can infer that `XMLPROP_ITEM` is in-fact a structure which contains these fields and this function is parsing these fields and matching their PKEYs for later use. One such example can be seen below where we see a load effective address on the PKEY for the "publisher" field.

![](./2021-06-17_16-09.png)

In this case we can see that the string "publisher" is loaded into the RAX register and then moved from RAX into RDI+10, this is followed by a load effective address of `PKEY_Company` and then a move from RAX into RDI+20.