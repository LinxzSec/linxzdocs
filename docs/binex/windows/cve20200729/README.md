# CVE-2020-0729: Remote Code Execution Through .LNK Files

While Trend Micro's research team were analysing a Windows patch bundle, they noticed a DLL which stood out to them: **StructuredQuery.dll**. The reason it stood out was because they had seen vulnerabilities in the past explicitly named as involving StructuredQuery such as [CVE-2018-0825](https://portal.msrc.microsoft.com/en-US/security-guidance/advisory/CVE-2018-0825). A search for StructuredQuery on Windows dev center revealed the documentation for [structuredquery.h header](https://docs.microsoft.com/en-us/windows/win32/api/structuredquery/), which told them that it was used by Windows Search, and this is precisely where LNK files and StructuredQuery connect.

[Original Article](https://www.zerodayinitiative.com/blog/2020/3/25/cve-2020-0729-remote-code-execution-through-lnk-files)

## Beginning Analysis

LNK files are mostly known for containing binary structures that create a shortcut to a file or folder. But a lesser known feature of LNK files is that they can directly contain a saved search. When a user searches for a file in Windows 10, the "Search Tools" tab appears in Explorer which allows a user to refine their search and select advanced options for that search. This tab also allows a user to save the existing search for re-use, which creates an XML file with the extension ".search-ms", this file format is only [partially documented](https://docs.microsoft.com/en-us/windows/win32/search/-search-savedsearchfileformat). 

However, they note that this is not the only way to save a search. If you click and drag the search results icon from the address bar to another folder, it creates a LNK file containing a serialized version of the data that would be contained in a "search-ms" XML file. With that in mind, we can look at the results of the patch dff for StructuredQuery using BinDiff.

![BinDiff](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585166967035-JWC4Y816DPPCKE0U1EJW/ke17ZwdGBToddI8pDm48kPkTQeDLDNV1Y0ofNr9fCUTlfiSMXz2YNBs8ylwAJx2qgRUppHe6ToX8uSOdETM-XipuQpH02DE1EkoTaghKW779xRs5veesYFcByqmynT9oi4UbVdpYZc7QyHblI0zTkk5XUKxxSSseuYQgYla_c0Y/Picture3.png?format=750w)

As shown in the above image, only one function changed: `StructuredQuery1::ReadPROPVARIANT()` and it appears to have changed quite extensively based on the similarity of only 81%.

Windows shell link files have several required and optional components as defined in the [Shell Link Binary File Format specification](https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-shllink/16cb4ca1-9339-4d0c-a68d-bf1d6cc0f943). Each shell link file must contain at a minimum, a Shell Link Header which has the following format.

![Shell Link Header](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585167064711-LYEZ10HTNLOW37UJTPF6/ke17ZwdGBToddI8pDm48kLlA15STVkgWd7e79p_iWgRZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZUJFbgE-7XRK3dMEBRBhUpyevV994hKESAMt_xLO45SWrsE2mNbDR8cz3BfgtGCqFzKBZNNvqA_y_p-bocYdqiI/Picture5.png?format=750w)

All multi-byte fields are represented in little-endian byte order unless specified otherwise. The LinkFlags field specifies the presence of absence of optional structures as well as various options such as whether the strings in the shell link are encoded in Unicode or not. The following image is an illustrative layout of the LinkFlags field.

![LinkFlags Field](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585167095424-RE6A09TLGUOJIXH0P3PS/ke17ZwdGBToddI8pDm48kHU90yU9WdcBEoyz1d3nZN5Zw-zPPgdn4jUwVcJE1ZvWEtT5uBSRWt4vQZAgTJucoTqqXjS3CfNDSuuf31e0tVGfCsKgaPymDHL0MosrIvfPdIQeYruw_O3_4k-XfbhdYzqWIIaSPh2v08GbKqpiV54/Picture6.png?format=500w)

One flag which is set in the majority of cases is, *HasLinkTargetIDList* which is represented by position "A", the least significant bit of the first byte of the LinkFlags field. If set, the LinkTargetIDList structure must follow the Shell Link Header. The LinkTargetIDLIst structure specifies the target of the link and has the following layout:

![LinkTargetIDList Structure](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585167123979-BAF56LDHGNG2XK2AXJZ8/ke17ZwdGBToddI8pDm48kDQ7ymkQ8D3nMs8VxDK4E9RZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PI28Aj2QJTKumFHm94Z0KiHQg7Wy15bNCi1yqdRJz9LAk/Picture7b.png?format=750w)

The IDList structure contained within specifies the format of a persisted item ID list:

![Persisted Item ID List](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585167150594-TGWNSHGLSH8CYNN5996Z/ke17ZwdGBToddI8pDm48kPiRCrvDXQ6lMTfUHzRM1GRZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PIPN9MQIjo6mUO066BvyeWjv8iBD3OXxI_mjkXmDP64uE/Picture8.png?format=750w)

The ItemIDList serves the same purpose as a file path, where each ItemID structure corresponds to one path component. ItemIDs can refer to actual filesystem folders, virtual folders such as Control Panel or other forms of embedded data that serve as a "shortcut" to perform a specific function. Particularly important to the vulnerability are the ItemIDList and ItemID structures present in an LNK file that contains a saved search query.

When a user creates a shortcut that contains search information, the resulting file contains an IDList structure that begins with a Delegate Folder ItemID, followed by a User Property View ItemID specific to search queries. In general, ItemID's begin with the following structure.

![ItemID Structure](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585168908869-P5OGSXYL2OE7TC43GV0Y/ke17ZwdGBToddI8pDm48kJzf76o5RmYX2GSHhd02R2dZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PIHOK05usGKpm-9nPfPMEex6MVI76N8BXlIHsky2-X8Sc/Picture9.png?format=750w)

The value of the two bytes beginning at offset 0x0004 are used in combination with the *ItemSize* and *ItemType* to help determine the type of the *ItemID*. For example, if the ItemSize is 0x14 and the ItemType is 0x1F, the 2 bytes at 0x0004 are checked to see if their value is greater than ItemSize. If so, it is assumed that the remaining ItemID data will consist of a 16-byte GUID. This is typically the structure of the first ItemID found in a LNK file pointing to a file or folder. If the ItemSize is larger than the size required to contain a GUID but smaller than the bytes at 0x0004, the remaining data after the GUID is considered an *ExtraDataBlock*, beginning with a 2-byte size field followed by that many bytes of data.

For a *Delegate Folder ItemID*, the same 2 bytes correspond to a size field for the remaining structure, leading to the following overall structure:

![Delegate Folder ItemID](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585168970007-ARGS9G2MWMM6FBINXOJ2/ke17ZwdGBToddI8pDm48kMBKOQ5S3VBFWKthLuauvVBZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PImh_LjL9k2Pj1M0JgVJeupj1Q5TP7zCuhhy0EmWOPdyQ/Picture10.png?format=750w)

All GUIDs in LNK files are stored using the [RPC IDL representation](https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-dtyp/49e490b8-f972-45d6-a3a4-99f924998d97) for GUIDs. RPC IDL representation means that the first three segments of the GUID are stored as little-endian representations of the entire segment (a DWORD followed by 2 WORDs), whereas each byte in the last 2 segments are considered to be individual.

For example, the GUID `{01234567-1234-ABCD-9876-0123456789AB}` has the binary representation: ` \x67\x45\x23\x01\x34\x12\xCD\xAB\x98\x76\x01\x23\x45\x67\x89\xAB`.

The precise function of Delegate Folder ItemIDs is not documented. However, it is likely that such an entry is intended to cause subsequen ItemIDs to be handled by the class specified by the Item GUID field, thus establishing that class as the root namespace for the hierarhcy. In the case of a LNK file containing embedded search data, the Item GUID will be: `{04731B67-D933-450A-90E6-4ACD2E9408FE}`, corresponding to `CLSID_SearchFolder` a reference to Windows.Storage.Search.dll.

The Delegate Folder ItemID is followed by a User Property View ItemID, which has a structure similar to the structure of a Delegate Folder ItemID:

![User Property View ItemID](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585169060536-5SQROARU17ZNEN5676ZE/ke17ZwdGBToddI8pDm48kM-YncN4a2zE93PO-gEpJR1Zw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZUJFbgE-7XRK3dMEBRBhUpwBi5s6RrTK1b4sseOr35O3j1ovmu8P86eR7-kf_ELEeww3-K-OI6Gv_KlAN5DGUUk/Picture11.png?format=750w)

The *PropertyStoreList* field is of particular importance to the vulnerability, if present contains one or more [serialized PropertyStore](https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-propstore/1eb58eb3-e7d8-4a09-ac0e-8bcb14b6fa0e) items each having the following structure:

![PropertyStore](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585169087879-VKTRKDK9H2G23TD736B4/ke17ZwdGBToddI8pDm48kG3a5GTA571MnBLy3oi-209Zw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZUJFbgE-7XRK3dMEBRBhUpydBLuwUnEl2cFBAReFyBuCb12QaoQEjYGxtS99zUjfIgWSQ28zvtx9M0v13sks1cQ/Picture12.png?format=750w)

The *Property Store Data field* is a sequence of properties. All properties in a given *PropertyStore* belong to the class identified by the *Property Format GUID*. Each specific property is identified by a numeric ID known as a *Property ID* or PID which, when combined with the *Property Format GUID*, is known as a *Property Key*. The PKEY is determined in a slightly different manner if the *Property Format GUID* is equal to `{D5CDD505-2E9C-101B-9397-08002B2CF9AE}`. Each property is then considered to be part of a "*Property Bag*" and has the following structure:

![Property Bag](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585169122328-PZK4GMWFS9CXJJSR3Z63/ke17ZwdGBToddI8pDm48kJ3IcjdnlbOdT871ubkyi-5Zw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PIXxWjgcGwY9Wc0rdX_wKrg4bGCHihRLr-bptUMwBPuHM/Picture13.png?format=750w)

Property Bags will generally contain elements with the names "Key:FMTID" and "Key:PID", identifying the specific PKEY that determines the interpretation of the other elements. Specific Property Bag Implementations will also require that other elements are present in order to be valid.

If the Property Format GUID is not equal to the previously mentioned GUID for Property Bags, each property is identified by an integer value for the PID and has the following structure:

![](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585169164006-TFP2DW6Z2JW4UIOUDBAL/ke17ZwdGBToddI8pDm48kCRqi0cylsqSlzs5PeS_Ua9Zw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PIJGb7gsC1ynj1vQT-X-r8uD0K7wVz-lHpYi2B3Xf4KSw/Picture14.png?format=750w)

The TypedPropertyValue field corresponds to the typed value of a property in a propery set as defined in section 2.15 of the [Microsoft Object Linking and Embedding OLE Property Set Data Structures](https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-oleps/f122b9d7-e5cf-4484-8466-83f6fd94b3cc) specification.

Various PKEYs are defined in the headers provided with the Windows SDK. However, many are undocumented and only identifiable by examining references in the debugging symbols for the associated libraries. For LNK files containing embedded search data, the first PropertyStore in the User Property View ItemID has a Property Format GUID of: `{1E3EE840-BC2B-476C-8237-2ACD1A839B22}` containing a property with an ID of 2, which corresponds to `PKEY_FilterInfo`.

The TypedPropertyValue field of *PKEY_FilterInfo* consists of a *VT_STREAM* property. Ordinarily, a VT_STREAM property consists of a type of 0x0042, 2 padding bytes, and an IndirectPropertyName that specifies the name of an alternate stream containing either a PropertySetStream packet for simple property set storage or the "CONTENTS" stream element for non-simple property set storage as per Microsofts [documentation](https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-oleps/e3bdb608-4756-4a0c-b2fd-15b7eee52905). This name is specified with the wide character string "prop" followed by a decimal string corresponding to a property identifier in a PropertySet packet. However, because LNK files use serialized property stores embedded in VT_STREAM properties, the IndirectPropertyName is only checked to see if it begins with "prop". The value itself is ignored. This results in the following TypedPropertyValue structure:

![TypedPropertyValue](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585169369408-3WH7H6STL6F7R34HDH5C/ke17ZwdGBToddI8pDm48kPc8dnjtrYJuO89GBlHo0XxZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZUJFbgE-7XRK3dMEBRBhUpyJE61e9NgIcEwdg_geIe6orlciZEMi62kg7LuYEsZZQBDcDbC6ZOLzUmp3qhAAN4k/Picture15.png?format=750w)

The contents of the Stream Data field are dependent on the specific PKEY that the stream property belongs to. For PKEY_FilterInfo, the Stream Data essentially contains an embedded PropertyStoreList with more serialized PropertyStore structures and has the following structure:

![PropertyStoreList](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585169458647-RKJMVGPFRPWS2TGOPCHG/ke17ZwdGBToddI8pDm48kMbfWfEKTr6B7OK0WZMy2E1Zw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PI0nK_OfiQh3DJOAbLQVvha_GnmjXHBq1J9U4vniqfJco/Picture16.png?format=750w)

The nested PropertyStoreList in the *PKEY_FilterInfo* stream is a serialized version of the "conditions" tag in a .search-ms file. The following is an example structure of the conditions tag:

![Conditions](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585169497086-J7Z9M7AUO07QUNRHU8R7/ke17ZwdGBToddI8pDm48kP_8jesKrUNbEyH5N-H9UvtZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PIpVHd5g-oqp3VdXM9DwItZZTB4VpEXkrnoc58Sh8x_e4/Picture17.png?format=750w)

The precise functionality of the **attribute** element is not publicly documented. However, an **attribute** elements contains a GUID that corresponds to **CONDITION_HISTORY**, and a CLSID that corresponds to the CConditionHistory class in StructuredQuery, which would imply that the nested condition and attribute structures represent the history of the search query before it was saved. In general, it appears that the **chs** attribute of the **attribute** element determines whether any additional history is present. When this structure is serialized into a property store, it is placed into the *PKEY_FilterInfo* PropertyStoreList, which takes the form of a propery bag with aforementioned Property Format GUID. More specifically, the serialized Conditions structure is contained in a VT_STREAM Property which is identified by the name "Condition". This results in a PropertyStore item having the following structure:

![PropertyStore Item](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585169561545-29H7GR4UP1RRYNPHJVGB/ke17ZwdGBToddI8pDm48kD_7dcyiSgcaaKeRIvzArtdZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZUJFbgE-7XRK3dMEBRBhUpzgCayD3qgRH32M1vrhmNDW80w89umtdEmBzW8Db9ReyjbPuen9h98CloHWt5y5ei4/Picture18.png?format=750w)

The Condition object is generally either a "Leaf Condition" or a "Compound Condition" object that contains a number of nested objects generally including one or more Leaf Condition objects and possibly additional Compound Condition objects. Both condition objects begin with the following structure:

![Condition Objects](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585169597575-S28ID4MGO9NRSXNKAXKM/ke17ZwdGBToddI8pDm48kG_HqR-MSX0ipiP-otatqC1Zw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PI8W_c2VjBRjPzQv1NO7OhHJPjHRBoowP-ZPwPjiH5IWA/Picture19.png?format=750w)

The Condition GUID will be `{52F15C89-5A17-48E1-BBCD-46A3F89C7CC2}` for a Leaf Condition and `{116F8D13-101E-4FA5-84D4-FF8279381935}` for a Compound Condition. The Attributes field consists of attribute structures, where the number of attributes structures is defined by the field "Number of Attributes". Each attribute structure corresponds to an **attribute** element from the .search-ms file and begins with the following structure:

![Attribute Element](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585169653287-FWVRB0OPYKQEMNU41CYK/ke17ZwdGBToddI8pDm48kIGSAn2sMNNy41jvw1lWX-dZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZUJFbgE-7XRK3dMEBRBhUpwJ77Z-uKxPXTtSK2oSwOZh0TmFxUQP8B0cyAyshkRTCV9iIRaPiSm7Xh8Y3nnHn2A/Picture20.png?format=750w)

The remaining structure of an Attribute depends on the AttributeID and CLSID. For the aforementioned **CONDITION_HISTORY** attribute, the attributeID is set to `{9554087B-CEB6-45AB-99FF-50E8428E860D}` and has a CLSID of `{C64B9B66-E53D-4C56-B9AE-FEDE4EE95DB1}`. The remaining structure will be a ConditionHistory object having the following structure. Note that the fields are named the same as the matching attributes of the **attribute** XML element.

![ConditionHistory](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585169739870-W110K940JV2AXXMIQIDN/ke17ZwdGBToddI8pDm48kOaqfLx17iCRYlTQu7a0Y9FZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PIQLKmm4GluksFoE2C9MMbTTH5LwK007IJ-TbDXfX3640/Picture21.png?format=750w)

If the value of `has_nested_condition` is greater than zero, the `CONDITION_HISTORY` attribute will have a nested condition object, which may itself have nested attributes that have nested conditions and so on.

Once the top-level attribute is fully read including all nested structures, the Compound Condition and Leaf Condition structures start to differ. The remaining structure of a Compound Condition is as follows, with offsets relative to the end of the Attributes field:

![Compound Condition](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585169786572-2TA6T142FA7KRC2Q5R1P/ke17ZwdGBToddI8pDm48kDIfWjaFtJzNAQzFh1baWOJZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PIlfcJ5KlY_9n0hwQCtrBN9_CqqMBcp2Fd5Rufy8yz8Bc/Picture22.png?format=750w)

The *numFixedObjects* field determines how many additional conditions (typically Leaf Conditions) will immediately follow. The remaining structure of a Leaf Condition is as follows, with offsets relative to the end of the Attributes field.

![Leaf Condition](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585169829378-OVKW64VON2BAH59N7ZDU/ke17ZwdGBToddI8pDm48kM1Lr9bAZ8MIWK_T9l5LiQtZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PI2f_lp_wzh_oZnZIVgSt5wkas5vmvFGMosTuaxnwBK-M/Picture23.png?format=750w)

The presence of the **TokenInformationComplete** structures depends on whether the preceding flag is set. If it is not set, the structure is not present and the next flag immediately follows. If it is set, the following structure is present:

![](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585169878262-HVD0FYNMNUWQ60KI52JT/ke17ZwdGBToddI8pDm48kHXSiVvzJCZsr3UtSSc0VvhZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZUJFbgE-7XRK3dMEBRBhUpxX0vfAf6kJNzEx1wGPRiSuHidLhQ1TPVSCazFRGmVDwGchGBfRA_3QPQmWPuRRtxc/Picture24.png?format=750w)

Im summary, the following tree shows the simplest possible structure of an LNK file with a save search, with irrelevant structures removed for simplicity:

![LNK file structure](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585169915247-79E5ASW3P8N6WTK8AUP7/ke17ZwdGBToddI8pDm48kCGfvITlU7I9fBSSmnmfXc5Zw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PI96PNVKlqW7f1sCV1dskDwVO455E_el_5mmtWD2funlk/Picture25.png?format=750w)

Keep in mind a search with a single Leaf Condition results in the simplest structure. More often than not, a saved search LNK file will begin with a Compound Condition and many nested structures including many Leaf Conditions.

## The Vulnerability

Now that we have explained the core structure of a saved search LNK file, we can take a look at the vulnerability itself, which lies in how the **PropertyVariant** field of Leaf Conditions are handled.

The PropertyVariant field of a Leaf Condition roughly corresponds to a [PROPVARIANT structure](https://docs.microsoft.com/en-us/windows/win32/api/propidl/ns-propidl-propvariant). PropertyVariant structures consist of a 2-byte type followed by data specific to that type. It is important to note that StructuredQuery appears to have a slightly custom implementation of the PROPVARIANT structure as the padding bytes specified in Microsoft's specification are generally not present.

Is is also important to note that a value of 0x1000, or VT_VECTOR, combined with another type means that there will be several values of the specified type.

Parsing of the PropertyVariant field is handled by the vulnerable function, **StructuredQuery1::ReadPROPVARIANT()**. The function first reads the 2-byte type and checks to see if VT_ARRAY (0x2000) is set, due to the fact it is not supported in StructuredQuery:

![](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585169987237-VDQGDPHDWH139NYMS2TI/ke17ZwdGBToddI8pDm48kDywRMCDe9bmcRoC1T6MHftZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PIH05v97oaSktft2-6P20CUqUJaN27lMyaM0pz6cMNAnE/Picture26.png?format=750w)

The function then checks if the type is VT_UI4 (0x0013) and if not, enters a switch statement to handle all other types. The vulnerability irself lies in how a PropertyVariant with a type of VT_VARIANT (0x000C) is handled. The VT_VARIANT type is typically used in combination with VT_VECTOR which effectively results in a series of PropertyVariant structures. In other words, this is like having an array where members of the array may be of any data type.

When the type of the PropertyVariant is set to VT_VARIANT (0x000C), the full type field is checked to see if VT_VECTOR is set.

![](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585170030771-QYXWIR9WPL8RAYTY3OPP/ke17ZwdGBToddI8pDm48kCfIBHK6aHRhShts9m-OGkFZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PIONt4rbmRKB7fNuY6R7HIS1YIg7T4mZipzFpfeTG-nog/Picture27.png?format=750w)

If VT_VECTOR is noet set, a 24 byte buffer is allocated with a call to **CoTaskMemAlloc()** and the buffer is passed to a recursive call to **ReadPROPVARIANT()**, with the intention that the buffer will be filled with the property that immediately follows the VT_VARIANT field. However, the buffer is not initialised (e.g. filled with NULL bytes) before it is passed to **ReadPROPVARIANT()**.

If the nested property has a type of VT_CF (0x0047), a property that is intended to contain a pointer to clipboard data, **ReadPROPVARIANT()** performs the same check for VT_VECTOR and if it is not set, attempts to write the next 4 bytes of the stream into a location pointed to by an 8-byte value in the previously allocated 24 byte buffer.

![](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585170096246-D27YZ48YQ2C2CZ1CG1QQ/ke17ZwdGBToddI8pDm48kCq1n0FpVAl-lhIN07y6-gxZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PIvPRDrzHP8CHzoRQjZ3p1_th6S8VREoiI4U6mrBJ_Jts/Picture28.png?format=750w)

Due to the fact that the buffer was not initialized, the data will be written to an undefined memory location, pontentially leading to arbitrary code execution. The attempted data write can be seen in the following exception and partial stack trace with Page Heap enabled on explorer.exe:

![](https://images.squarespace-cdn.com/content/v1/5894c269e4fcb5e65a1ed623/1585170128175-URU450Z04SJOE8VPHFTU/ke17ZwdGBToddI8pDm48kOSjtr03dPVXbGHRWc5ONshZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PI1_IhYz4DzekxWtW3GK5ryZTQ9PqDhB57yTg6NDTbPn8/Picture29.png?format=750w)

Essentially, if an attacker can manipulate the memory layout in just the right way so that the uninitialized buffer contains a value they control, they can write any data 4 bytes at a time to a memory address of their choosing.

## Conclusion

In order to resolve this vulnerability, the solution is simple. Fill the newly allocated 24-byte buffer with NULL bytes ensuring that an attacker is unable to utilize data in the buffer leftover from the previous uses of that memory location.