
# Windows Kernel

[[toc]]

# Virtualization-Based Security (VBS)

Virtualization-based Security, VBS, uses hardware virtualization features to create and isolate a secure region of memory from the normal operating system. Windows implements a number of technologies under this banner of "VBS", the most important of which being the idea of a "Virtual Secure Mode" which itself houses a number of security solutions.

## Virtual Secure Mode (VSM)

Virtual Secure Mode is a set of hypervisor capabilities offered to host and guest partitions which enables the creation and management of new security boundaries within operating system software. VSM is the new hypervisor facility on which Windows security features including: Device Guard, Credential Guard, virtual TPMs and shielded VMs are based.

VSM enables operating system software in the root and guest partitions to create isolated regions of memory for storage and processing of system security assets. Access to these isolated regions is controlled and granted solely through the hypervisor, which is a highly privileged and highly trusted part of the system's Trust Computer Base (TCB).

Because the hypervisor runs at a higher privilege level than operating system software and has exclusive control of key system hardware resources such as memory access permission controls in the CPU MMU and IOMMU early in system initilization, the hypervisor can protect these isolated regions of from unauthorized access, even from operating system software (OS Kernel and device drivers) with supervisor mode access (ring 0).

With this architecture, even if normal system level software running in supervisor mode is compromised by malicious software, the assets in isolated regions protected by the hypervisor can remain secured.

#### VSM Detection and Status

The VSM capability is advertised to partitions via the `AccessVsm` partition privilege flag. Only partitions with **all** the following privileges may utilize VSM: 

- `AccessVsm`
- `AccessVpRegisters` 
- `AccessSynicRegs`

### Virtual Trust Level (VTL)

VSM achieves and maintains isolation through Virtual Trust Levels (VTLs). VTLs are enabled and managed on both a per-partition and per-virtual processor basis.

VTLs are hierarchical, with higher levels being more privileged than lower ones. VTL0 is the least privileged level with VTL1 being more privileged. Up to 16 levels of VTLs are supported. Currently, only two VTLs are implemented

```C
typedef UINT8 HV_VTL, *PHV_VTL;

#define HV_NUM_VTLS 2
#define HV_INVALID_VTL ((HV_VTL) -1)
#define HV_VTL_ALL 0xF
```

Each VTL has its own set of memory access protections. These access protections are managed by the hypervisor in a partition's physical address space, and thus cannot be modified by system level software running in the partition.

Since more privileged VTLs can enforce their own memory protections, higher VTLs can effectively protect areas of memory from lower VTLs. In practice this allows a lower VTL to protect isolated memory regions by securing them with a higher VTL. As an example, VTL0 could store a secret in VTL1 at which point only VTL1 could access that secret. This ensures that even if VTL0 is compromised, the secret would be safe.

#### VTL Protections

There are multiple stages to achieving isolation between VTLs:

- **Memory Access Protections:** Each VTL maintains a set of guest physical memory access protections. Software running at a particular VTL can only access memory in accordance with these protections.
- **Virtual Processsor State:** Virtual processors maintain separate per-VTL state. For example, each VTL defines a set of private VP registers. Software running at a lower VTL cannot access the higher VTLs private virtual processor's register state.
- **Interrupts:** Along with a separate processor state, each VTL also has its own interrupt subsystem (local APIC). This allows higher VTLs to process interrupts without risking interference from a lower VTL.
- **Overlay Pages:** Certain overlay pages are maintained per-VTL such that higher VTLs have reliable access, i.e, there is a separate hypercall overlay page per VTL.


### Isolated User Mode (IUM)

Windows 10 introduced a new security feature named "Virtual Secure Mode". VSM leverages the Hyper-V hypervisor and Second Level Address Translation (SLAT) to create a set of modes known as "Virtual Trust Levels" (VTL). This new software architecture creates a security boundary to prevent processes running in one VTL from accessing the memory of another VTL. The benefit of such isolation is the additional mitigation of kernel exploits and protection of assets such as password hashes and Kerberos keys.

The diagram below depicts the traditional model of Kernel mode and User mode code running in CPU ring 0 and ring 3. In this new model, the code running in the traditional model executes in VTL0 and it cannot access the higher privileged VTL1, where the Secure Kernel and IUM execute code. The VTLs are hierarchical meaning any code running in VTL1 is more privileged than code running in VTL0.

![IUM](https://docs.microsoft.com/en-us/windows/win32/procthread/images/uim-architecture.png)


The `SYSTEM_ISOLATED_USER_MODE_INFORMATION` structure is not documented however it is possible to find most of the structure from [Geoff Chappell's blog](https://www.geoffchappell.com/studies/windows/km/ntoskrnl/api/ex/sysinfo/isolated_user_mode.htm).

`SYSTEM_ISOLATED_USER_MODE_INFORMATION` is produced in the output buffer by a successful call to either of the following functions:

- `ZwQuerySystemInformation` or `NtQuerySystemInformation;`
- `ZwQuerySystemInformationEx` or `NtQuerySystemInformationEx;`


#### Trustlets

Truslets (also known as trusted processes, secure processes or IUM processes) are programs running as IUM processes in VSM. They complete syscalls by marshalling them over to the NT Kernel (VTL0). VSM creates a small execution environment that includes the Small Secure Kernel. The clear security benefit is isolation of trustlet user mode pages in VTL1 from drivers running in the NT Kernel. Even if the NT kernel is compromised, it will not have access to the IUM process page.

With VSM enabled, the Local Security Authority (LSASS) environment runs as a trustlet. LSASS manages the local system policy, user authentication and auditing while handling sensitive security data such as password hashes and Kerberos keys. To leverage the security benefits of VSM, a trustlet named *lsaiso.exe* (LSA Isolated) runs in VTL1 and communicates with *lsass.exe* running in VTL0 through an RPC channel. The LSAISO secrets are encrypted before sending them over to LSASS running in VTL0 and the pages of LSAISO are protected from malicious code running in VTL0.

![Truslet Design](https://docs.microsoft.com/en-us/windows/win32/procthread/images/lsass-trustlet-design.png)

### Hypervisor Code Integrity (HVCI)

### Kernel Data Protection (KDP)