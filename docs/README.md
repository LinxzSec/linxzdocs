---
home: true
heroText: Notes
footer: I believe this is usually for something about licenses and copyright.
---

::: warning Disclaimer
Please keep in mind that most of the content here is just notes! It won't be coherent, that well organised or professionally written.
:::

# Welcome

This site is my personal collection of notes for things that interest me. If you're looking for more coherent writings you can check out my blog linked in the navigation bar.

If you're wondering, this is VuePress! Nothing fancy, although "back-to-top" is installed for usability purposes. Feel free to clone the respository from [GitLab](https://gitlab.com/LinxzSec/linxzdocs).

# Contact

You can find all of my contact information on my blog in the navigation bar. But if you're lazy, here's my Jabber and Keybase.

- **Jabber:** Linxz@jabber.calyxinstitute.org
- **Keybase:** Linxz
