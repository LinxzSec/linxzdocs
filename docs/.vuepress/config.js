module.exports = {
    title: 'LinxzDocs',
    description: 'A collection of notes on CyberSec',

	plugins: ['@vuepress/back-to-top'],

    themeConfig: {

     smoothScroll: true,

     displayAllHeaders: true,
     sidebarDepth: 2,
     sidebar: 'auto',

     nav: [
       	{ text: 'Home', link: '/' },
       	{ text: 'Blog', link: 'https://linxz.tech' },

		{ text: 'Kernels',
	 	 items: [
	  		{ text: 'Windows', items: [
	   	 		{ text: 'Virtualization', link: '/kernel/windows/virtualization/' },
	    		{ text: 'CVEs', link: '/kernel/windows/cve/' },
          ]},
	  		{ text: 'Linux', items: [
	    		{ text: 'Virtualization', link: '/kernel/linux/virtualization/' },
	    		{ text: 'CVEs', link: '/kernel/linux/cve/' }
          	]}
	 	]},

       { text: 'Binex',
	 	 items: [
	  		{ text: 'Windows', items: [
				{ text: 'CVE-2020-0729', link: '/binex/windows/cve20200729/' },
				{ text: 'Library-MS Analysis', link: '/binex/windows/librarymsanalysis/' },
	    		{ text: 'Kernelspace', link: '/binex/windows/kernelspace/' }
          	]},
	  		{ text: 'Linux', items: [
	    		{ text: 'Userspace', link: '/binex/linux/userspace/' },
	    		{ text: 'Kernelspace', link: '/binex/linux/kernelspace/' }
          	]}
		 ]},
		 
		{ text: 'Architecture',
		 items: [
			{ text: 'Virtualization', items: [
				{ text: 'SLAT', link: '/binex/windows/userspace/' },
			   	{ text: 'Kernelspace', link: '/binex/windows/kernelspace/' },
			 ]},
			{ text: 'Operating Systems', items: [
			   { text: 'Qubes', link: '/binex/linux/userspace/' },
			   { text: 'Kernelspace', link: '/binex/linux/kernelspace/' }
			 ]}
		]},

      { text: 'Cryptography',
		ariaLabel: 'Cryptography Menu',
		items: [
	 		{ text: 'Crypto', link: '/crypto/' }
		]}
      ]
    }
}
